# Roof Detection

Esse projeto tem o objetivo identificar telhados.
## DATASET
- [dataset_rooftop (contém 3000 imagens)](https://drive.google.com/drive/folders/1vjoMNuyrsR-ylPXrziUGSYd2IJ7BgS--?usp=sharing)

## Documentação
- [Project Charter](https://gitlab.com/BDAg/roofdetection/wikis/project-charter)
- [Cronograma](https://gitlab.com/BDAg/roofdetection/wikis/Cronograma)
- [Mapa de Conhecimento](https://gitlab.com/BDAg/roofdetection/wikis/Mapa-de-Conhecimento)
- [Mapa de Definição Tecnológica](https://gitlab.com/BDAg/roofdetection/wikis/Mapa-de-Defini%C3%A7%C3%A3o-Tecnol%C3%B3gica)
- [Matriz de Habilidades](https://gitlab.com/BDAg/roofdetection/wikis/Matriz-de-Habilidades)
- [Team](https://gitlab.com/BDAg/roofdetection/wikis/Team)
- [Lista de funcionalidades](https://gitlab.com/BDAg/roofdetection/wikis/Lista-de-funcionalidades)
- [Modelagem de Dados](https://gitlab.com/BDAg/roofdetection/wikis/Modelagem-de-Dados)
- [Ambiente de Desenvolvimento](https://gitlab.com/BDAg/roofdetection/wikis/Ambiente-de-Desenvolvimento)
- [MVP - Minimal Viable Product](https://gitlab.com/BDAg/roofdetection/wikis/MVP-Minimal-Viable-Product)
- [Arquitetura da Solução](https://gitlab.com/BDAg/roofdetection/wikis/Arquitetura-da-Solução)
- [Diagrama de telas](https://gitlab.com/BDAg/roofdetection/wikis/Diagrama-de-telas)
- [Armazenamento de imagem](https://gitlab.com/BDAg/roofdetection/wikis/armazenamento-de-imagem) 