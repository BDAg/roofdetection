from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import numpy as np
import tensorflow as tf

from tensorflow.contrib import learn
from tensorflow.contrib.learn.python.learn.estimators import model_fn as model_fn_lib

#matplotlib inline
import matplotlib.pyplot as plt
import numpy as np
import scipy.ndimage as ni
import scipy.misc as mi
from PIL import Image
from scipy import misc
import glob
import cv2
import os

tf.logging.set_verbosity(tf.logging.INFO)

def cnn_model_fn(features,labels,mode):
  #Input layer
  input_layer = tf.reshape(features, [-1,128,128,3])
  
  #Convolution layer #1
  conv1 = tf.layers.conv2d(
    input_layer,
    filters=28,
    kernel_size=[5,5],
    padding="same",
    activation=tf.nn.relu)
  
  #Pooling layer #1
  pool1=tf.layers.max_pooling2d(
    inputs=conv1,
    pool_size=[2,2],
    strides=2)
  
  #Convolution layer #2
  conv2 = tf.layers.conv2d(
    pool1,
    filters=14,
    kernel_size=[5,5],
    padding="same",
    activation=tf.nn.relu)
  
  #Pooling layer #2
  pool2=tf.layers.max_pooling2d(
    inputs=conv2,
    pool_size=[2,2],
    strides=2)
  
  
  #Convolution layer #3
  conv3 = tf.layers.conv2d(
    pool2,
    filters=7,
    kernel_size=[5,5],
    padding="same",
    activation=tf.nn.relu)
  
  #Pooling layer #3
  pool3=tf.layers.max_pooling2d(
    inputs=conv3,
    pool_size=[2,2],
    strides=2)
  # Dense Layer
  pool2_flat = tf.reshape(pool3, [-1, 16 * 16 * 7])
  dense = tf.layers.dense(
    inputs=pool2_flat,
    units=512,
    activation=tf.nn.relu)
  dense2 = tf.layers.dense(
    inputs=dense,
    units=256,
    activation=tf.nn.relu)
  dense3 = tf.layers.dense(
    inputs=dense2,
    units=128,
    activation=tf.nn.relu)
  dense4 = tf.layers.dense(
    inputs=dense3,
    units=64,
    activation=tf.nn.relu)
  dense5 = tf.layers.dense(
    inputs=dense4,
    units=32,
    activation=tf.nn.relu)
  dense6 = tf.layers.dense(
    inputs=dense5,
    units=16,
    activation=tf.nn.relu)
  dropout = tf.layers.dropout(
    inputs=dense6,
    rate=0.4,
    training=mode == learn.ModeKeys.TRAIN)

  # Logits Layer
  logits = tf.layers.dense(
    inputs=dropout,
    units=4)

  loss = None
  train_op = None

  # Calculate Loss (for both TRAIN and EVAL modes)
  if mode != learn.ModeKeys.INFER:
    onehot_labels = tf.one_hot(
      indices=tf.cast(labels, tf.int32),
      depth=4)
    loss = tf.losses.softmax_cross_entropy(
      onehot_labels=onehot_labels,
      logits=logits)

  # Configure the Training Op (for TRAIN mode)
  if mode == learn.ModeKeys.TRAIN:
    train_op = tf.contrib.layers.optimize_loss(
      loss=loss,
      global_step=tf.contrib.framework.get_global_step(),
      learning_rate=0.001,
      optimizer="SGD")

  # Generate Predictions
  predictions = {
    "classes": tf.argmax(
      input=logits,
      axis=1),
    "probabilities": tf.nn.softmax(
      logits,
      name="softmax_tensor")
  }

  # Return a ModelFnOps object
  return model_fn_lib.ModelFnOps(mode=mode, predictions=predictions, loss=loss, train_op=train_op)
    
def main(unused_argv):
  train_dir='./training/'
  image_path=""
  train_images=[]
  train_labels=[]

  for image_path in glob.glob(train_dir+"*.PNG"):
    os.rename(image_path,image_path.replace(".PNG",".png"))

  for image_path in glob.glob(train_dir+"*.png"):
    if 'Flat' in image_path:
      #print(1)
      train_images.append(cv2.resize(cv2.imread(image_path),(128,128)))
      train_labels.append(np.array(0))
    elif 'Gable' in image_path:
      train_images.append(cv2.resize(cv2.imread(image_path),(128,128)))
      train_labels.append(np.array(1))
    elif 'Hip' in image_path:
      train_images.append(cv2.resize(cv2.imread(image_path),(128,128)))
      train_labels.append(np.array(2))
    elif 'Gambrel' in image_path:
      train_images.append(cv2.resize(cv2.imread(image_path),(128,128)))
      train_labels.append(np.array(3))
  
  train_data=np.float32(np.asarray(train_images))
  train_labels=np.float32(np.asarray(train_labels))
  
  test_dir='./test/'
  test_images=[]
  test_labels=[]

  for image_path in glob.glob(test_dir+"*.PNG"):
    os.rename(image_path,image_path.replace(".PNG",".png"))

  for image_path in glob.glob(test_dir+"*.png"):
    if 'Flat' in image_path:
      test_images.append(cv2.resize(cv2.imread(image_path),(128,128)))
      test_labels.append(np.array(0))
    elif 'Gable' in image_path:
      test_images.append(cv2.resize(cv2.imread(image_path),(128,128)))
      test_labels.append(np.array(1))
    elif 'Hip' in image_path:
      test_images.append(cv2.resize(cv2.imread(image_path),(128,128)))
      test_labels.append(np.array(2))
    elif 'Gambrel' in image_path:
      test_images.append(cv2.resize(cv2.imread(image_path),(128,128)))
      test_labels.append(np.array(3))

  eval_data=np.float32(np.asarray(test_images))
  eval_labels=np.float32(np.asarray(test_labels))
  # Create the Estimator
  building_classifier = learn.Estimator(model_fn=cnn_model_fn)
  # Set up logging for predictions
  tensors_to_log = {"probabilities": "softmax_tensor"}
  logging_hook = tf.train.LoggingTensorHook(
    tensors=tensors_to_log,
    every_n_iter=50)
  building_classifier.fit(
    x=train_data,
    y=train_labels,
    batch_size=35,
    steps=10000,
    monitors=[logging_hook])
  # Configure the accuracy metric for evaluation
  metrics = {"accuracy":learn.MetricSpec(
    metric_fn=tf.metrics.accuracy,
    prediction_key="classes"),}
  eval_results = building_classifier.evaluate(
    x=eval_data,
    y=eval_labels,
    metrics=metrics)
  print(eval_results)

if __name__ == "__main__":
    tf.app.run()

import cv2

test_dir='./training/'
image_path=""
test_images=[]
test_labels=[]

for image_path in glob.glob(test_dir+"*.png"):
  test_images.append(cv2.resize(cv2.imread(image_path),(128,128)))
  test_labels.append(np.array(1))

test_images=np.float32(np.asarray(test_images))
test_images[0]
input_layer=tf.reshape(test_images, [-1,128,128,3])
print(input_layer)    
print("                          || ||                             ")
print("                          \\\\\\ //                             ")
print("                           \\ /                             ")
#Convolution layer #1
#Convolution layer #1
conv1 = tf.layers.conv2d(
  input_layer,
  filters=28,
  kernel_size=[5,5],
  padding="same",
  activation=tf.nn.relu)
print(conv1)   
    #Pooling layer #1
pool1=tf.layers.max_pooling2d(
  inputs=conv1,
  pool_size=[2,2],
  strides=2)
print(pool1)    
print("                          || ||                             ")
print("                          \\\\\\ //                             ")
print("                           \\ /                             ")
#Convolution layer #2
conv2 = tf.layers.conv2d(
  pool1,
  filters=24,
  kernel_size=[5,5],
  padding="same",
  activation=tf.nn.relu)
print(conv2) 
##Pooling layer #2
pool2=tf.layers.max_pooling2d(
  inputs=conv2,
  pool_size=[2,2],
  strides=2)
print(pool2)    
print("                          || ||                             ")
print("                          \\\\\\ //                             ")
print("                           \\ /                             ")    
#Convolution layer #3
conv3 = tf.layers.conv2d(
  pool2,
  filters=7,
  kernel_size=[5,5],
  padding="same",
  activation=tf.nn.relu)
print(conv3)  
#Pooling layer #3
pool3=tf.layers.max_pooling2d(
  inputs=conv3,
  pool_size=[2,2],
  strides=2)
print(pool3)
print("                          || ||                             ")
print("                          \\\\\\ //                             ")
print("                           \\ /                             ")
pool2_flat = tf.reshape(pool3, [-1, 16 * 16 * 7])
print(pool2_flat)
print("                          || ||                             ")
print("                          \\\\\\ //                             ")
print("                           \\ /                             ")
dense = tf.layers.dense(
  inputs=pool2_flat,
  units=512,
  activation=tf.nn.relu)
print(dense)
dense2 = tf.layers.dense(
  inputs=dense,
  units=256,
  activation=tf.nn.relu)
print(dense2)
dense3 = tf.layers.dense(
  inputs=dense2,
  units=128,
  activation=tf.nn.relu)
print(dense3)
dense4 = tf.layers.dense(
  inputs=dense3,
  units=64,
  activation=tf.nn.relu)
print(dense4)
dense5 = tf.layers.dense(
  inputs=dense4,
  units=32,
  activation=tf.nn.relu)
print(dense5)
dense6 = tf.layers.dense(
  inputs=dense5,
  units=16,
  activation=tf.nn.relu)
print(dense6)
dropout = tf.layers.dropout(
  inputs=dense6,
  rate=0.4)
print(dropout)
print("                          || ||                             ")
print("                          \\\\\\ //                             ")
print("                           \\ /                             ")
# Logits Layer
logits = tf.layers.dense(
  inputs=dropout,
  units=4)
print(logits)

# Load training and eval data
mnist = learn.datasets.load_dataset("mnist")
train_data = mnist.train.images # Returns np.array
train_labels = np.asarray(mnist.train.labels, dtype=np.int32)
eval_data = mnist.test.images # Returns np.array
eval_labels = np.asarray(mnist.test.labels, dtype=np.int32)
train_data.shape